package database;

import Base.Environment;

import  java.sql.Connection;
import  java.sql.Statement;
import  java.sql.ResultSet;
import  java.sql.DriverManager;
import  java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by vladyslav on 8/11/17.
 */
public class DataBaseConnector {

  public static Connection con;
   static Statement stmt;



        public static  void  setDataBaseConnection() throws  ClassNotFoundException, SQLException {


            String  dbUrl = Environment.databaseUrl;
            //String  dbUrl = "jdbc:mysql://aiouat-rds.aiouat.ipfdigital.io/loanengine";

            //Database Username
            String username = Environment.databaseUsername;
            //String username = "testteam";


            //Database Password
            String password = Environment.databasePassword;
            //String password = "7Pq5vralVpP77TPU";



            //Load mysql jdbc driver
            Class.forName("com.mysql.jdbc.Driver");

            //Create Connection to DB
             con = DriverManager.getConnection(dbUrl,username,password);

            //Create Statement Object
             stmt = con.createStatement();

        }


    public static  String getUserId(String ssn)throws  Throwable{

            //Execute statement

            String query = "Select ID from `Customer` Where `identifier`="+ssn;
            ResultSet rs=stmt.executeQuery(query);
            String id="";
            while (rs.next()){
                id = rs.getString(1);
            }
            return id;
        }


        public static String getUserEmail (String id)throws  Throwable{
            //Execute statement

            String query = "Select email from `Customer` Where `ID`="+id;
            ResultSet rs=stmt.executeQuery(query);
            String email="";
            while (rs.next()){
                email = rs.getString(1);
            }
            return email;
        }



        public static void verifyBankAccount (String ssn)throws Throwable{
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            String closeTime= "('" + dateFormat.format(date)+"')";
            String identifier="('"+ssn + "')";
            String query = "UPDATE `UserRegistrationRequest` SET `state`='VERIFIED'," +
                    " identificationStatus='IDENTIFIED'," +
                    " `previousIdentificationStatus`='PENDING_IDENTIFICATION'," +
                    " `closed`="+closeTime+  " Where `identifier`="+identifier;
            stmt.executeUpdate(query);
        }



    public static String getCreditApplication(String id)throws  Throwable{

        //Execute statement

        String query = "SELECT ID from `CreditApplication` Where `customer_ID`="+id;
        ResultSet rs=stmt.executeQuery(query);
        String ca="";
        while (rs.next()){
            ca = rs.getString(1);
        }
        return ca ;
    }




    public static String getBKAresponse (String creditApplicationId)throws  Throwable{
        String query = "select status from CreditApplicationExternalData where creditApplication_ID="+creditApplicationId;
        ResultSet rs=stmt.executeQuery(query);
        String status="";
        while (rs.next()){
            status = rs.getString(1);
        }
        return status ;

    }

    public static String getCreditApplicationCase(String creditApplication)throws  Throwable{

        //Execute statement

        String query = "select cac.ID from CreditApplicationCase cac join AbstractCase ac on ac.id=cac.id where cac.creditApplication_ID="+creditApplication;


        ResultSet rs=stmt.executeQuery(query);
        String cac="";
        while (rs.next()){
            cac = rs.getString(1);
        }
        return cac ;
    }


    public static String getDrawDownCase(String creditApplication)throws  Throwable{
            String query = "select ac.ID from NewDrawdownCase ndc join AbstractCase ac on ac.id=ndc.id where ac.state IN (\"OPEN\") and ndc.creditApplication_ID="
                    + creditApplication;

        ResultSet rs=stmt.executeQuery(query);
        String drawCase="";
        while (rs.next()){
            drawCase = rs.getString(1);
        }
        return drawCase ;

    }


    public static String getContractId(String customerId)throws  Throwable{

            String query = "Select ID from Contract Where `customer_ID`="+customerId;

        ResultSet rs=stmt.executeQuery(query);
        String contractId="";
        while (rs.next()){
            contractId = rs.getString(1);
        }
        return contractId ;
    }


    public static List<String> getSelfServiceData(String id) throws Throwable{
        //Execute statement

        String query = "Select msisdn,email, bankAccount from `Customer` Where `ID`="+id;
        ResultSet rs=stmt.executeQuery(query);
        List<String> results = new ArrayList<String>();

        // While Loop to iterate through all data and print results

        while (rs.next()){
            //String res=rs.getString(1);
            results.add(rs.getString(1));
            results.add(rs.getString(2));
            results.add(rs.getString(3));

        }

        return results;

    }




    public void rejectCase(String caseId)throws Throwable{
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String closeStamp= "('" + dateFormat.format(date)+"')";

        String query = "Update `AbstractCase` SET `closeStamp`=" + closeStamp+ ", `closedById`=('?'), state=('REJECTED') Where ID="+caseId;
        stmt.executeUpdate(query);
    }



    public void rejectApplication(String applicationID)throws Throwable{
        //DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Date date = new Date();
        //String closeStamp= "('" + dateFormat.format(date)+"')";

        String query = "Update `CreditApplication` SET state=('REJECTED') Where ID IN (".concat(applicationID).concat(")");
        stmt.executeUpdate(query);
    }


    public static void closeDatabaseConnection () throws SQLException{
        // closing DB Connection
        if (con!=null){
            con.close();
        }

    }




    }



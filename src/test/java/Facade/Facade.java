package Facade;

import org.openqa.selenium.WebDriver;
import pages.application.*;
import pages.externalResources.DataGeneratorPage;
import pages.registration.BankVerificationPageLT;
import pages.registration.RegistrationPageLT;
import pages.mainPage.MainPageLT;
import pages.selfService.SelfServiceLT;
import pages.application.products.CreditLinePage;
import pages.application.products.InstallmentLoanPage;


/**
 * Created by vladyslav on 7/26/17.
 */
public class Facade {

     public WebDriver driver;

    public Facade(WebDriver driver) {
        this.driver = driver;
    }

    public MainPageLT mainPage(){
        return new MainPageLT(driver);
    }

    public SelfServiceLT selfServ () {return new SelfServiceLT(driver);}

    public RegistrationPageLT registration() {
        return new RegistrationPageLT(driver);
    }

    public BankVerificationPageLT bankPage (){
        return new BankVerificationPageLT(driver);
    }

    public GeneralInfoPage generalInfo (){

        return new GeneralInfoPage(driver);
    }

    public FinancialInfoPage financePage(){
        return new FinancialInfoPage(driver);
    }

    public ProductSelectionPage productsPage(){
        return new ProductSelectionPage(driver);

    }

    public CreditLinePage creditLinepage (){
        return new CreditLinePage(driver);
    }

    public InstallmentLoanPage installment(){
        return new InstallmentLoanPage(driver);
    }


    public LegalPage legalPage(){
        return new LegalPage(driver);
    }

    public ThankYouPage thankYouPage() {return new ThankYouPage(driver);}

    public DataGeneratorPage dataGen(){
        return new DataGeneratorPage(driver);
    }


}

package pages.selfService;

import Recorders.Log;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BasePageObject;
import users.RegisteredUser;

import java.util.List;

/**
 * Created by vladyslav on 7/28/17.
 */
public class SelfServiceLT extends BasePageObject {

    public SelfServiceLT (WebDriver driver){
        super(driver);
    }

    private   By overview = By.id("overview-tab");
    private   By personalInfo = By.xpath(".//span[text()='Bendra informacija']/../..");
    private   By userInfoTable=By.xpath(".//h3[text()='Jūsų asmeninė informacija']/../table");
    private  By changeDetails=By.cssSelector(".changePassword.btn");
    private  By personalDetailsForm=By.xpath(".//form[@name='form']");
    private  By amountToDraw=By.xpath(".//*[@class='slider' ][1]//span");

/*
Extra services
 */
    private  By takePaymentHoliday = By.xpath(".//button[@ng-switch-when='PAYMENT_HOLIDAY']");
    private  By changeDueDate=By.xpath(".//button[@ng-switch-when='CHANGE_DUE_DATE']");
    private  By upgrade=By.xpath(".//button[@ng-switch-when='UPGRADE_CREDITLINE']");
    private  By upgradeSelectionPopUp = By.xpath("//div[@ng-controller='InitCreditlineUpgradeCtrl']");

/*
    @FindBy(xpath="//button[@ng-click='initUpsell()']")
    WebElement upsell;
*/

    private  By upsell=By.xpath("//button[@ng-click='initUpsell()']");


    private  By goToNewApplication=By.xpath("//button[@ng-show='showGotoApplication']");
    private By invoicesTab = By.xpath(".//span[@class='ng-binding' and text()='SĄSKAITOS IR MOKĖJIMAI']");
    private By invoicesInfo=By.id("invoices-tab");


    public boolean selfServicePageIsShown(){

        try {

            return waitIsShown(overview).size()>0;
        }
        catch (TimeoutException ex){
            return false;
        }

    }

    public void openPersonalInfo(){
        interactWith(personalInfo).click();
    }

    public boolean personalInfoIsDisplayed (){
      try {
          return waitIsShown(userInfoTable).size()>0;
      }
      catch (TimeoutException ex){
          return false;
      }

    }


    public void openDetailsForm (){
        interactWith(changeDetails).click();

    }

    public boolean formIsOpened(){
        try {
           return waitIsShown(personalDetailsForm).size()>0;
        }
        catch (Exception ex){
            ex.printStackTrace();
            return false;
        }

    }


    public void openInvoicesTab(){

        interactWith(invoicesTab).click();
    }

    public boolean invoiceTabIsOpened(){
        try {
            return waitIsShown(invoicesInfo).size()>0;
        }
        catch (Exception ex){
            ex.printStackTrace();
            return false;
        }

    }

    public void changeEmail(){
        interactWith(By.xpath(".//*[@id='epEmail']")).clear();

        String randomString= RandomStringUtils.randomAlphabetic(5);
        String email=randomString.concat("@ipfdigital.com");
        interactWith(By.xpath(".//*[@id='epEmail']")).sendKeys(email);
        RegisteredUser.email=email;
    }

    public String getEmailShownInSelfService (){
        return waitToBeShown(By.cssSelector(".email")).getText();
    }

    public String getPhoneShownInSelfService (){
        return waitToBeShown(By.cssSelector(".msisdn")).getText();
    }

    public String getBankNumberShownInSelfService(){

        return waitToBeShown(By.cssSelector(".bankAccount")).getText();
    }


    public String getAddressShownInSelfService (){

        return waitToBeShown(By.cssSelector(".address")).getText();

    }




    public void confirmChanges (){
        interactWith(By.xpath(".//input[@value='Išsaugoti']")).click();
        interactWith(By.xpath(".//input[@value='Taip']")).click();
    }


    public void setNewPhone(){
        String phone=RandomStringUtils.randomNumeric(7);
        interactWith(By.xpath(".//*[@name='msisdn']")).clear();
        interactWith(By.xpath(".//*[@name='msisdn']")).sendKeys("+");
        interactWith(By.xpath(".//*[@name='msisdn']")).sendKeys(phone);
        interactWith(By.xpath(".//button[text()='GAUTI SMS KODĄ']")).click();
        interactWith(By.xpath(".//*[@id='epOtp']")).sendKeys("1234");
        RegisteredUser.phone="+3706".concat(phone);

    }


    public void changeBankNumber(String bankNumber){
        interactWith(By.id("epBankAccount")).clear();
        interactWith(By.id("epBankAccount")).sendKeys(bankNumber);
        RegisteredUser.bank=bankNumber;
    }


    public void changeAddress(){
        interactWith(By.name("livingAddress")).clear();
        String newAddress = RandomStringUtils.randomAlphabetic(8).concat(" ").concat(RandomStringUtils.randomNumeric(2));
        interactWith(By.name("livingAddress")).sendKeys(newAddress);
        RegisteredUser.address=newAddress;
    }



    public void enterCurrentPassword(String password){
        interactWith(By.xpath(".//*[@id='epСurrentPassword']")).clear();
        interactWith(By.xpath(".//*[@id='epСurrentPassword']")).sendKeys(password);

    }


    public void setNewPassword(String newPassword){
        interactWith(By.xpath(".//*[@id='epPassword']")).clear();
        interactWith(By.xpath(".//*[@id='epPassword']")).sendKeys(newPassword);
        interactWith(By.xpath(".//*[@id='epPassRepeat']")).clear();
        interactWith(By.xpath(".//*[@id='epPassRepeat']")).sendKeys(newPassword);
    }

    public int getAmountToDraw(){

        wait.until(ExpectedConditions.textToBePresentInElementLocated(amountToDraw,"€"));
        String str = waitToBeShown(amountToDraw).getText().replaceAll("\\s+","").replace("€","");

        return   Integer.parseInt(str);

    }

    public void increaseDraw(){
        interactWith(By.xpath(".//div[@class='plus']")).click();
    }

    public void decreaseDraw (){
        interactWith(By.xpath(".//div[@class='minus']")).click();
    }


    public void setAmountToDraw(int amount){
        //int amount=500;
        if(amount< getAmountToDraw()){
            while (amount < getAmountToDraw()){
                decreaseDraw();
                getAmountToDraw();
            }
        }
        else if (amount> getAmountToDraw()){
            while ( amount > getAmountToDraw()){
                increaseDraw();
                getAmountToDraw();
            }
        }
    }

    public void drawMoney(int amount){
        setAmountToDraw(amount);
        interactWith(By.xpath(".//button[text()='PERSIVESTI PINIGUS']")).click();
        interactWith(By.xpath(".//input[@value='Taip']")).click();
    }

    public boolean sliderIsShown(){

        try {
            return waitIsShown(By.xpath(".//div[@class='slider']")).size()>0;

        }
        catch (TimeoutException ex){
            Log.info("Draw slider IS NOT shown");
            return false;
        }

    }

    public boolean paymentHolidayIsAvailable (){
        WebDriverWait waiting=new WebDriverWait(driver,3);
        try {
            waiting.until(ExpectedConditions.elementToBeClickable(takePaymentHoliday));
            Log.info("Payment Holiday IS available");
            return true;
        }
        catch (TimeoutException tex){
            Log.info("Payment Holiday IS NOT available");
            return false;
        }

    }


    public boolean changeDueDateIsAvailable (){
        WebDriverWait waiting=new WebDriverWait(driver,3);
        try {
            waiting.until(ExpectedConditions.elementToBeClickable(changeDueDate));
            Log.info("Change DD IS available");
            return true;

        }
        catch (TimeoutException tex){
            Log.info("Change DD is NOT available");
            return false;

        }
    }

    public boolean upgradeIsAvailable (){
        WebDriverWait waiting=new WebDriverWait(driver,3);
        try {
            waiting.until(ExpectedConditions.elementToBeClickable(upgrade));
            Log.info("UPGRADE IS available");
            return true;
        }
        catch (TimeoutException tex){
            Log.info("UPGRADE IS NOT available");
            return false;
        }
    }


    public boolean newApplicationButtonIsShown(){
      try {
          return waitIsShown(goToNewApplication).size()>0;
      }
      catch (TimeoutException ex){
          return false;
      }
    }

    public void upgradeCreditLine(){

        interactWith(upgrade).click();
    }

    public void upsell(){
        interactWith(upsell).click();
    }


    public boolean upgradePopUpIsShown(){

        try {
            return waitIsShown(upgradeSelectionPopUp).size()>0;
        }
        catch (TimeoutException ex){
            return false;
        }
    }

    public void selectNewProductLimit(){

        waitIsShown(upgradeSelectionPopUp)
                .get(0)
                .findElements(By.xpath("//div[@class='checker ng-scope']/label[@class='ng-binding']"))
                .get(0)
                .click();

    }

    public void submitProductChange(){
        interactWith(By.xpath("//input[@class='btn accept']")).click();
    }

    public void applyForProduct(){
        interactWith(goToNewApplication).click();
    }

}

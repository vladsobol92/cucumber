package pages.application;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BasePageObject;

/**
 * Created by vladyslav on 8/11/17.
 */
public class LegalPage extends BasePageObject {
    public LegalPage(WebDriver driver) {
        super(driver);
    }

    public boolean LegalPageIsShown (){
        WebDriverWait waiting = new WebDriverWait(driver, 40);
        waiting.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='application-progress']")));
        return waiting.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//table[@class='ng-scope']"))).isDisplayed();

    }

    public void acceptLegalTerms (){

        ScrollToElement(By.xpath(".//label[@for='agreementCheckbox']"));
        interactWith(By.xpath(".//label[@for='agreementCheckbox']")).click();
    }

}

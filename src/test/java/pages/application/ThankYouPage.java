package pages.application;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.BasePageObject;

/**
 * Created by vladyslav on 8/11/17.
 */
public class ThankYouPage extends BasePageObject {

    public ThankYouPage(WebDriver driver) {
        super(driver);
    }

    public void finishApplication(){
        interactWith(By.xpath(".//button[@type='submit'][1]")).click();
    }
}

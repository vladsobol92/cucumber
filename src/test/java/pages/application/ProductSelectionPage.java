package pages.application;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BasePageObject;
import pages.application.products.CreditLinePage;
import pages.application.products.InstallmentLoanPage;

/**
 * Created by vladyslav on 8/7/17.
 */
public class ProductSelectionPage extends BasePageObject {

  private  By ILbtn= By.xpath(".//*[@id='product-tabs']/li[2]");
   private By CLbtn=By.xpath(".//*[@id='product-tabs']/li[1]");

    public By amountToDraw(){
        return null;
    }
    public By addMoney(){
        return null;
    }

    public By subtractMoney (){
        return null;
    }

    public ProductSelectionPage(WebDriver driver) {
        super(driver);
    }





    public boolean productSelectionPageIsDisplayed (){
        WebDriverWait waiting = new WebDriverWait(driver, 40);
        waiting.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='application-progress']")));
        return waiting.until(ExpectedConditions.attributeContains(By.xpath(".//ul[@class='progress']/li[3]"),
                "class","active"))
                && driver.findElement(By.xpath(".//ul[@class='progress']/li[3]"))
                .getText()
                .contains("SUTARTIES SUDARYMAS");
    }


    public CreditLinePage selectCreditLine(){
        try {
            interactWith(CLbtn).click();
        }

        catch (Exception ex){
            ex.printStackTrace();
        }

        return new CreditLinePage(driver);
    }


    public InstallmentLoanPage selectInstallment (){
        try {
            interactWith(ILbtn).click();
        }

        catch (Exception ex){
            ex.printStackTrace();
        }
        return new InstallmentLoanPage(driver);
    }



    public int getAmountToDraw(){

        wait.until(ExpectedConditions.textToBePresentInElementLocated(amountToDraw(),"€"));
        String str = waitToBeShown(amountToDraw()).getText().replaceAll("\\s+","").replace("€","");


        return   Integer.parseInt(str);

    }


    public void increaseDraw(){
        interactWith(addMoney()).click();
    }

    public void decreaseDraw (){
        interactWith(subtractMoney()).click();
    }


    public void setAmountToDraw(int amount){
        if(amount< getAmountToDraw()){
            while (amount < getAmountToDraw()){
                decreaseDraw();
                getAmountToDraw();
            }
        }
        else if (amount> getAmountToDraw()){
            while ( amount > getAmountToDraw()){
                increaseDraw();
                getAmountToDraw();
            }
        }
    }
}

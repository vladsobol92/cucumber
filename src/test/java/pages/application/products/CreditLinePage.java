package pages.application.products;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import pages.application.ProductSelectionPage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vladyslav on 8/3/17.
 */
public class CreditLinePage extends ProductSelectionPage {

    By CLslider = By.xpath(".//div[@class='slider']/div[@ng-model='CLDrawSelected']");
    By addMoney= By.xpath(".//div[@class='slider']/div[@ng-model='CLDrawSelected']/div[@class='plus']");
    By subtractMoney= By.xpath(".//div[@class='slider']/div[@ng-model='CLDrawSelected']/div[@class='minus']");
    By amountToDraw = By.xpath(".//div[@class='slider']/div[@ng-model='CLDrawSelected']/..//span");
    By creditLineLimit = By.xpath(".//select[@ng-model='selectedCLProductId']");

    public CreditLinePage(WebDriver driver) {
        super(driver);
    }


    public boolean creditLineSelectionIsDisplayed(){
        try {
            return waitIsShown(CLslider).size()>0;

        }
        catch (Exception ex){
            return false;
        }
    }

    @Override
    public By amountToDraw(){
        return amountToDraw;
    }

    @Override
    public By addMoney(){
        return addMoney;
    }

    @Override
    public By subtractMoney(){
        return subtractMoney;
    }


    public void setLimit (int limit){
        if (limit <= 750){
            String text=Integer.toString(limit).concat(" €");
            Select limits = new Select(interactWith(creditLineLimit));
            limits.selectByVisibleText(text);
        }
        else {
            String text=Integer.toString(limit);
            String firstNumber= Character.toString(text.charAt(0));
            String amount= firstNumber.concat(" 000 €");
            Select limits = new Select(interactWith(creditLineLimit));
            limits.selectByVisibleText(amount);
        }


    }


}

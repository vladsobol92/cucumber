package pages.application.products;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.application.ProductSelectionPage;

/**
 * Created by vladyslav on 8/6/17.
 */
public class InstallmentLoanPage extends ProductSelectionPage {

    By ILdrawSlider= By.xpath(".//div[@class='slider']/div[@ng-model='ILAmountSelected']");
    By maturitySlider= By.xpath(".//div[@class='slider']/div[@ng-model='ILMaturitySelected']");
    By maturityPeriod = By.xpath(".//div[@class='slider']/div[@ng-model='ILMaturitySelected']/..//span");
    By increaseMaturity=By.xpath(".//div[@class='slider']/div[@ng-model='ILMaturitySelected']/div[@class='plus']");
    By decreaseMaturity=By.xpath(".//div[@class='slider']/div[@ng-model='ILMaturitySelected']/div[@class='minus']");
    By addMoney=By.xpath(".//div[@class='slider']/div[@ng-model='ILAmountSelected']/div[@class='plus']");
    By subtractMoney=By.xpath(".//div[@class='slider']/div[@ng-model='ILAmountSelected']/div[@class='minus']");
    By amountToDraw = By.xpath(".//div[@class='slider']/div[@ng-model='ILAmountSelected']/..//span");

    public InstallmentLoanPage(WebDriver driver) {
        super(driver);
    }

    public boolean installmentSelectionIsDisplayed(){
        return waitToBeShown(ILdrawSlider).isDisplayed() && waitToBeShown(maturitySlider).isDisplayed() ;
    }

    @Override
    public By amountToDraw(){
        return amountToDraw;
    }

    @Override
    public By addMoney(){
        return addMoney;
    }

    @Override
    public By subtractMoney(){
        return subtractMoney;
    }

    public int getMaturity(){
        String str = waitToBeShown(maturityPeriod).getText().replaceAll("\\s+","").replace("mėn.","");
        return   Integer.parseInt(str);

    }


    public void increaseMaturity(){
        interactWith(increaseMaturity).click();
    }

    public void decreaseMaturity (){
        interactWith(decreaseMaturity).click();
    }


    public void setMaturityPeriod(int month){
        if(month< getMaturity()){
            while (month < getMaturity()){
                decreaseMaturity();
                getMaturity();
            }
        }
        else if (month> getAmountToDraw()){
            while ( month > getMaturity()){
                increaseMaturity();
                getMaturity();
            }
        }
    }

}

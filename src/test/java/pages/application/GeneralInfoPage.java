package pages.application;

import Recorders.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import pages.BasePageObject;
import pages.constants.Education;
import pages.constants.Residence;
import users.RegisteredUser;

/**
 * Created by vladyslav on 8/2/17.
 */
public class GeneralInfoPage extends BasePageObject {


    By education = By.xpath(".//div[@name='education']/select");
    public  By residence = By.xpath(".//div[@name='residence']/select");
    By livingAddress = By.name("livingAddress");
    By bankAccount=By.xpath(".//input[@name='bankAccount']");
    By submitGeneralInfo = By.xpath(".//button[@type='submit']");



    public GeneralInfoPage(WebDriver driver) {
        super(driver);
    }

    public boolean generalInfoPageDisplayed (){
        waitToBeShown(By.xpath(".//*[@id='application-progress']"));
        System.out.println(driver.findElement(By.xpath(".//ul[@class='progress']/li[1]")).getText());
        return wait.until(ExpectedConditions.attributeContains(By.xpath(".//ul[@class='progress']/li[1]"),"class","active"))&&
        driver.findElement(By.xpath(".//ul[@class='progress']/li[1]")).getText().contains("BENDRA INFORMACIJA");

    }


    public void setEducation (Education educ){

     WebElement education= interactWith(this.education);
     Select eduType = new Select(education);
     eduType.selectByValue(educ.getValue());

    }

    public void setResidence (Residence res){

        WebElement residence = interactWith(this.residence);
        Select resType = new Select(residence);
        resType.selectByValue(res.getValue());
    }

    public void setBankNumber (String bankNumber){
        WebElement bank= interactWith(bankAccount);
        bank.clear();
        bank.sendKeys(bankNumber);
        RegisteredUser.setBankAccount(bankNumber);

        Log.info("User Bank Number: " + RegisteredUser.bank);
    }

    public void setAddress(String address){
        WebElement addressInput=interactWith(livingAddress);
        addressInput.clear();
        addressInput.sendKeys(address);
        RegisteredUser.address=address;
    }

    public void acceptTerms (){
        scroll(450);
        acceptRegistryPermissions();
    }


}

package pages.application;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import pages.BasePageObject;
import pages.constants.Bank;
import pages.constants.Employment;
import pages.constants.EmploymentDuration;


/**
 * Created by vladyslav on 8/2/17.
 */
public class FinancialInfoPage extends BasePageObject {

    private By emplType=By.xpath(".//div[@ng-model='selectedEmploymentType']/select");
    private By netIncome=By.xpath(".//input[@name='netIncome']");
    private By incomePeriod = By.xpath(".//div[@ng-model='selectedIncomePeriod']/select");
    private By monthlyObl=By.xpath(".//input[@name='monthlyObligations']");
    private By submitInfo = By.xpath(".//button[@type='submit']");

    public FinancialInfoPage(WebDriver driver) {
        super(driver);
    }

    public boolean financialPageIsDisplayed (){
        waitToBeShown(By.xpath(".//*[@id='application-progress']"));
        return  wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(".//ul[@class='progress']/li[2]"), "FINANSINĖ INFORMACIJA"));

                /*
        driver.findElement(By.xpath(".//ul[@class='progress']/li[2]"))
                .getText()
                .contains("FINANSINĖ INFORMACIJA");
      && wait.until(ExpectedConditions.attributeContains(By.xpath(".//ul[@class='progress']/li[2]"),
                "class","active"))

                */
    }

    public void setEmploymentType (Employment employment){
        WebElement type = interactWith(emplType);
        Select employmentTypes = new Select(type);
        employmentTypes.selectByValue(employment.getValue());
    }

    public void setIncome (int income){
        String monthlyIncome = Integer.toString(income);
        interactWith(netIncome).sendKeys(monthlyIncome);
    }



    public void setIncomePeriodEnums(EmploymentDuration duration){

        Select periodOfIncome = new Select(interactWith(incomePeriod));
        periodOfIncome.selectByValue(duration.getValue());
        



    }

    public void setMonthlyObligations (int amount){
        String sum=Integer.toString(amount);
        interactWith(monthlyObl).sendKeys(sum);

    }



}

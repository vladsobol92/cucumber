package pages.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.BasePageObject;

/**
 * Created by vladyslav on 8/1/17.
 */
public class BankVerificationPageLT extends BasePageObject {

    public BankVerificationPageLT(WebDriver driver) {
        super(driver);
    }

    public boolean bankVerificationPageDisplayed (){

        try {
         return    waitIsShown(By.xpath(".//*[@id='application-progress']")).size()>0

                 && wait.until(ExpectedConditions.attributeContains(By.xpath(".//ul[@class='progress']/li[2]"),"class","active"));
        }
        catch (Exception ex){
            return false;
        }

    }

    public void confirmBank (){

        ScrollToElement(By.cssSelector(".next.animate"));
        interactWith(By.cssSelector(".next.animate")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//input[@value='Taip']"))).click();
        String refresh=driver.getCurrentUrl();
        driver.get(refresh);




    }

}

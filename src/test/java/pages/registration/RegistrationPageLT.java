package pages.registration;

import Recorders.Log;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import pages.BasePageObject;
import pages.constants.Bank;
import users.RegisteredUser;

import java.util.List;

/**
 * Created by vladyslav on 7/29/17.
 */
public class RegistrationPageLT extends BasePageObject {



    private By otpPopUp = By.xpath(".//*[@class='popup-a']/div[1]");


    public RegistrationPageLT(WebDriver driver) {
        super(driver);
    }

    private By banks=By.xpath(".//*[@id='application-progress']//select");
    private  By registrationProgress = By.xpath(".//ul[@class='progress']/li[1]");
    private By submitRegistrationData = By.xpath(".//button[@type='submit']");


    public boolean registrationPageDisplayed(){
        try {
           return waitIsShown(registrationProgress).size()>0 && wait.until(ExpectedConditions.attributeContains(registrationProgress,"class","active"));
        }
        catch (Exception ex){
            return false;
        }

    }

    public void setIdCode (String idCode){


        interactWith(By.xpath(".//input[@name='ssn']")).sendKeys(idCode);
        RegisteredUser.setIdCode(idCode);
        Log.info("Users SSN : " + RegisteredUser.idCode);


    }

/*
Get idCodes for LT from http://172.0.3.163:8080/LT

    public void getIdcodeFromExtResource(){
        driver.get("http://172.0.3.163:8080/LT");
       List<WebElement> ssnCodes = waitIsShown(By.xpath("//tbody/tr/td[4]"));
       RegisteredUser.setIdCode(ssnCodes.get(1).getText());


    }
   */

    public void setPassword (String password){

        interactWith(By.xpath(".//input[@name='password']")).sendKeys(password);
        interactWith(By.xpath(".//input[@name='passwordRepeat']")).sendKeys(password);
        RegisteredUser.setPassword(password);
    }


    //For registration with default Qwerty123 password

    public void setDefaultPassword() {
        String password = "Qwerty123";
        interactWith(By.xpath(".//input[@name='password']")).sendKeys(password);
        interactWith(By.xpath(".//input[@name='passwordRepeat']")).sendKeys(password);
        RegisteredUser.setPassword(password);

    }

    public void setBankAccount (Bank bank){


        WebElement banks = driver.findElement(this.banks);

        Select bankList = new Select(banks);
        bankList.selectByValue(bank.getValue());
        RegisteredUser.setBankAccount(bank.getValue());

    }

    public void setPhone (String phone){
        WebElement phoneField = driver.findElement(By.xpath(".//input[@name='msisdn']"));

        phoneField.sendKeys(phone);
        RegisteredUser.setPhone(phone);

    }

/*
For registration with random number
-------------------------------------
*/
    public void setRandomPhone(){
        String phone= RandomStringUtils.randomNumeric(7);
        WebElement phoneField = driver.findElement(By.xpath(".//input[@name='msisdn']"));

        phoneField.sendKeys(phone);
        RegisteredUser.setPhone("+3706".concat(phone));
        Log.info("User phone: "+RegisteredUser.phone);

    }

/*
For registration with specific email
-------------------------------------
*/
    public void setMail (String email){

        WebElement emailField = interactWith(By.xpath(".//input[@name='email']"));
        emailField.clear();
        emailField.sendKeys(email);
        emailField.clear();
        emailField.sendKeys(email);
        RegisteredUser.setEmail(email);

        Log.info("User E-mail: " + RegisteredUser.email);

    }

/*
For registration with randomly generated email
-------------------------------------
*/
    public void setRandomEmail (){
        WebElement emailField = interactWith(By.xpath(".//input[@name='email']"));
        emailField.clear();
        String randomString= RandomStringUtils.randomAlphabetic(5);
        String email=randomString.concat("@ipfdigital.com");
        interactWith(By.xpath(".//input[@name='email']")).sendKeys(email);
        RegisteredUser.setEmail(email);

        Log.info("User E-mail: " + RegisteredUser.email);

    }


/*
Request One Time Password
-------------------------------------
*/
    public void requestOtp() {
        interactWith(By.cssSelector(".btn.request-otp")).click();
    }

/*
Close POP UP with one time password
-------------------------------------
*/
    public void closeOtpPopUp (){

        try {
            waitToBeShown(By.xpath(".//*[@class='popup-a']/div[1]/a")).click();
        }
       catch ( TimeoutException tex){
           tex.printStackTrace();
           throw new AssertionError("NO POP UP window is shown");
       }
    }

/*
Verify if pop that appeares after OTP request says that user is already registered in the system
-------------------------------------
*/
    public boolean isUserRegistered () throws AssertionError{

        String popUpText = interactWith(otpPopUp).getText();

         return popUpText.contains("Mobilusis numeris ar asmens kodas jau registruotas");
    }


/*
Enter OTP
-------------------------------------
*/
    public void setOtp() {

        interactWith(By.xpath(".//input[@name='otp']")).sendKeys("1234");

    }

/*
Click "Nesu" for political check
-------------------------------------
*/
    public void setPolitician(boolean politician){

        By politicalCheckBox=politician?(By.xpath(".//label[@for='pepConsent_no']")):(By.xpath(".//label[@for='pepConsent_yes']"));
        //ScrollToElement(By.xpath(".//label[@for='pepConsent_yes']"));

        ScrollToElement(politicalCheckBox);

       interactWith(politicalCheckBox).click();


    }

/*
Accept registry permissions and marketing permissions
-------------------------------------
*/
    public void acceptTerms () {

        acceptRegistryPermissions();


        ScrollToElement(By.xpath(".//label[@for='marketingPermission']")).click();



    }

/*
Submit registration data
-------------------------------------
*/
    public BankVerificationPageLT submitApplication (){
        interactWith(submitRegistrationData).click();
        return new BankVerificationPageLT(driver);
    }

}

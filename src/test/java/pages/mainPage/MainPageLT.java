package pages.mainPage;

import Base.Helpers;
import Recorders.Log;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BasePageObject;
import pages.selfService.SelfServiceLT;
import pages.registration.RegistrationPageLT;

/**
 * Created by vladyslav on 7/26/17.
 */
public class MainPageLT extends BasePageObject {



    private  By closePopUpbtn=By.xpath(".//*[@id='intro-popup']/div/button");
    private  By loginButton = By.id("login-button");
    private  By loginWindow = By.id("login");
    private  By loginField = By.id ("loginUsername");
    private  By passwordField = By.id("loginPassword");
    private  By forgotPassBtn=By.className("forgot_password");
    private  By submit = By.cssSelector(".button.login-button");
    private  By passwordRecWind=By.className("popup-a");
    private  By newPass=By.id("password");
    private  By repeatNewPass=By.id("repeatPassword");
    private  By registrationBtn=By.xpath(".//*[@id='login']/div[3]/div/p/a");


    public MainPageLT(WebDriver driver){
        super(driver);
    }


    public boolean mainPageIsShown(){
        return waitToBeShown(By.xpath(".//*[@id='app-widget']")).isDisplayed();
    }

/*
Close window POP up window that appears on the main page
___________________________
 */
    public void closeTipsWindow (){
        try {
            driver.findElement(closePopUpbtn).click();
        }
        catch (NoSuchElementException noel){
        }
    }
/*
Close cookies message on the bottom of the screen
___________________________
*/
    public void closeCookie(){
        try {
            scroll(150);
            interactWith(By.cssSelector(".close-cookie")).click();
        }
        catch (Exception ex){
            Log.info("NO cookie message");
        }
    }

/*
Open log in form
___________________________
*/
    public void openLoginForm (){
        driver.findElement(loginButton).click();
    }


/*
Verifying if Login/password window is displayed
___________________________
*/
    public boolean logInWindowIsDisplayed(){
        try {
            return  waitIsShown(loginWindow).size()>0;
        }
        catch (Exception ex){
            return false;
        }


    }

    public void setLogin(String code){
        driver.findElement(loginField).clear();
        driver.findElement(loginField).sendKeys(code);

    }

    public void setPassword (String password){
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
    }

    public SelfServiceLT submitLogIn(){
        driver.findElement(submit).click();
        return new SelfServiceLT(driver);
    }

    public void openPassRecoverWindow (){
        driver.findElement(forgotPassBtn).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordRecWind));
    }

    public boolean isPopUpDisplayed(){

        try {
            return waitToBeShown(passwordRecWind).isDisplayed();
        }
        catch (Exception ex) {
            return false;
        }

    }

    public void requestPassRecovery(String login){

        WebElement recoveryInput=driver.findElement(By.xpath(".//input[@id='passwordRecovery']"));


        recoveryInput.clear();
        recoveryInput.sendKeys(login);
        driver.findElement(By.cssSelector(".button.accept")).click();

        waitToBeShown(By.xpath(".//*[@class='popup-a']"));

        driver.findElement(By.xpath(".//*[@class='close']")).click();
    }


    public void  setNewPass (String newPass){
        WebElement newPassField=driver.findElement(this.newPass);
        newPassField.clear();
        newPassField.sendKeys(newPass);

    }

    public void repeatNewPass (String newPass){

        WebElement repeatPass=driver.findElement(repeatNewPass);
        repeatPass.clear();
        repeatPass.sendKeys(newPass);
    }

    public String popUpHeader (){
       return waitToBeShown(passwordRecWind).findElement(By.tagName("h2")).getText();
    }


    public SelfServiceLT submitNewPass (String newPass){
        setNewPass(newPass);
        repeatNewPass(newPass);
        driver.findElement(By.cssSelector(".btn.accept")).click();
        return new SelfServiceLT(driver);
    }

    public RegistrationPageLT openApplication (){
        driver.findElement(registrationBtn).click();
        return new RegistrationPageLT(driver);
    }


}

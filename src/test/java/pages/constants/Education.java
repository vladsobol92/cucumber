package pages.constants;

/**
 * Created by vladyslav on 8/4/17.
 */
public enum Education {
    UNIVERSITY("UNIVERSITY"), POLYTECHNIC("POLYTECHNIC"), VOCATIONAL("VOCATIONAL"),
    HIGH("HIGH"), MIDDLE("MIDDLE"), PRELIMINARY("PRELIMINARY");



    private String edu;

    Education(String edu){
        this.edu=edu;
    }

    public String getValue(){
        return edu;
    }


    }

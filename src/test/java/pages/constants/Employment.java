package pages.constants;

/**
 * Created by vladyslav on 8/6/17.
 */
public enum Employment {

    FULLTIME("FULL_TIME"), FIXEDTERM ("FIXED_TERM"), PARTTIME("PART_TIME"),
    ENTREPRENEUR("ENTREPRENEUR"),GOVERNMENT("GOVERNMENT"),RETIRED("RETIRED"),
    DISABILITY("DISABILITY_RETIRED"), STUDENT("STUDENT"), OTHER("OTHER");



    private String employment;

    Employment (String employment){
        this.employment=employment;
    }

    public String getValue(){
        return employment;
    }
}

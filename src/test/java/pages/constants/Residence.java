package pages.constants;

/**
 * Created by vladyslav on 8/4/17.
 */
public enum Residence {

    RENT("RENT"), OWNERHOUSE("OWNERHOUSE"), OWNERAPARTMENT("OWNERAPARTMENT"), WITHPARENTS("WITHPARENTS"), DORM("DORM");


    private String res;

    Residence(String res){
        this.res=res;
    }

    public String getValue(){
        return res;
    }





}

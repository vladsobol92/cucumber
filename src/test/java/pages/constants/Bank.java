package pages.constants;

/**
 * Created by vladyslav on 8/1/17.
 */
public enum Bank {

    SWEDBANK ("swed"), SEB ("seb"), DNB ("dnb"), NORDEA ("nordea"), CITADELE ("citadele"), CIAULIU ("siauliu");

    private String bank;

    Bank(String bank){
        this.bank=bank;
    }

    public String getValue(){
        return bank;
    }


}

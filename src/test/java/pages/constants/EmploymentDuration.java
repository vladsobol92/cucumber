package pages.constants;

/**
 * Created by vladyslav on 8/6/17.
 */
public enum EmploymentDuration {

    UPTOTHREEMONTHS("FROM_0_TO_3_MONTHS"),UPTOYEAR("FROM_4_TO_12_MONTHS"),MORETHENYEAR("FROM_13_AND_MORE_MONTHS");


    private String duration;
    EmploymentDuration (String duration){
            this.duration=duration;
        }

    public String getValue(){
            return duration;
        }


    }




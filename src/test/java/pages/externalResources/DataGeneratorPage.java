package pages.externalResources;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.BasePageObject;

import java.util.*;

/**
 * Created by vladyslav on 10/24/17.
 */
public class DataGeneratorPage extends BasePageObject {

   public static List<String> externalUserData;

   private By Lithuania = By.xpath("//div[@class='tab']/button[@class='tablinks LT-tab']");
   private By dataRow = By.xpath("//div[@id='LT-data']/div[@class='column'][1]//input");

    public DataGeneratorPage(WebDriver driver) {
        super(driver);
    }

    public void getDataGenerator(){
        driver.get("http://172.0.3.163:8080/generated-data");
    }

    public void selectCountry (){

        interactWith(Lithuania).click();

    }



    public void setUserData (){
        getDataGenerator();
        selectCountry();
        List <String> data = new ArrayList<String>();
        List <WebElement> dataWebElements = waitIsShown(dataRow);
        Iterator<WebElement> iterator = dataWebElements.iterator();
        while (iterator.hasNext()){
           data.add(iterator.next().getAttribute("value"));
        }

        externalUserData = data;
        System.out.println(externalUserData);
    }



}

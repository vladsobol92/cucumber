package pages;

import Base.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.mainPage.MainPageLT;


/**
 * Created by vladyslav on 7/27/17.
 */
public class BasePageObject extends Helpers {




   By logoutBtn = By.id("logout-button");
   By registryPermissions = By.xpath(".//label[@for='registryConsent']");
   By submitData = By.xpath(".//button[@type='submit']");

    public BasePageObject(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,30);
    }

    public MainPageLT logOut (){
        interactWith(logoutBtn).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login-button")));
        return new MainPageLT(driver);
    }



    public void acceptRegistryPermissions (){

        ScrollToElement(registryPermissions);
        interactWith(registryPermissions).click();

    }

    public void submit (){
        ScrollToElement(submitData);
        interactWith(submitData).click();

    }



}

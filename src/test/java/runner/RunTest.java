package runner;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;

/**
 * Created by vladyslav on 7/27/17.
 */

//@RunWith(Cucumber.class)
@CucumberOptions (plugin = {"pretty","html:target/Reports"} ,
        features = {"src/test/resoursec/features/LT" },
        tags = "@automate", glue="steps")
public class RunTest extends AbstractTestNGCucumberTests {
}

package steps;

import Base.BaseUtils;
import Recorders.Log;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import database.DataBaseConnector;
import org.testng.Assert;
import pages.externalResources.DataGeneratorPage;
import soap.Case;
import users.RegisteredUser;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by vladyslav on 8/23/17.
 */
public class SelfServiceSteps extends BaseUtils {

    BaseUtils base;

    public SelfServiceSteps(BaseUtils base) {

        this.base=base;
    }

    
    @Given("^User log in with customerId \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void userLogInWithCustomerIdAndPassword(String custId, String pass) throws Throwable {

        facade.mainPage().openLoginForm();
        facade.mainPage().setLogin(custId);
        RegisteredUser.customerId=custId;
        facade.mainPage().setPassword(pass);
        RegisteredUser.password=pass;
        facade.mainPage().submitLogIn();
        Assert.assertTrue(facade.selfServ().selfServicePageIsShown(),"Self Service page is shown");
    }

    @When("^User opens personal details$")
    public void userOpensPersonalDetails() throws Throwable {
        facade.selfServ().openPersonalInfo();
        Assert.assertTrue(facade.selfServ().personalInfoIsDisplayed());
    }

    @And("^User opens details form$")
    public void userOpensDetailsForm(){
        facade.selfServ().openDetailsForm();
        Assert.assertTrue(facade.selfServ().formIsOpened());
    }

    @And("^User changes email$")
    public void userChangesEmail() throws Throwable {

        userOpensDetailsForm();
        facade.selfServ().changeEmail();
        Log.info("New email: "+RegisteredUser.email);
        facade.selfServ().confirmChanges();
       // facade.selfServ().refreshPage();
     //   Assert.assertTrue(facade.selfServ().selfServicePageIsShown());


    }

    @And("^User changes phone$")
    public void userChangesPhone() throws Throwable {
       // facade.selfServ().openPersonalInfo();
        userOpensDetailsForm();
        facade.selfServ().setNewPhone();
        Log.info("New Phone: "+RegisteredUser.phone);

        facade.selfServ().confirmChanges();
      //  facade.selfServ().refreshPage();
    }



    @And("^User changes bankNumber$")
    public void userChangesBankNumber() throws Throwable {
        userOpensDetailsForm();
        facade.selfServ().changeBankNumber(DataGeneratorPage.externalUserData.get(6));
        Log.info("New bank number: " + RegisteredUser.bank);

        facade.selfServ().confirmChanges();
        facade.selfServ().refreshPage();
    }

    @Then("^Data is changed$")
    public void dataIsChanged() throws Throwable {

        facade.selfServ().openPersonalInfo();

        /*
          Wait until data is replicated to DB
         */
        Thread.sleep(3000);

        System.out.println(RegisteredUser.getSelfServiceData());

        Log.info("User phone in Self Service page: " + facade.selfServ().getPhoneShownInSelfService()  );
        Log.info("User E-mail in self service: " + facade.selfServ().getEmailShownInSelfService()  );
        Log.info("User bank number in self service: " + facade.selfServ().getBankNumberShownInSelfService());

        Log.info("User data from DB: " + DataBaseConnector.getSelfServiceData(RegisteredUser.customerId) );

        System.out.println(DataBaseConnector.getSelfServiceData(RegisteredUser.customerId));

        Assert.assertTrue(RegisteredUser.getSelfServiceData().containsAll(DataBaseConnector.getSelfServiceData(RegisteredUser.customerId)));

        Assert.assertEquals(RegisteredUser.getPhone(),facade.selfServ().getPhoneShownInSelfService());

        Assert.assertEquals(RegisteredUser.getEmail(),facade.selfServ().getEmailShownInSelfService());



        Assert.assertEquals(RegisteredUser.getBank(), facade.selfServ().getBankNumberShownInSelfService());




    }


    @When("^User requests draw (\\d+)$")
    public void userRequestsDraw(int amount) throws Throwable {
      facade.selfServ().drawMoney(amount);

    }

    @Then("^New drawdown case is opened$")
    public void newDrawdownCaseIsOpened() throws Throwable {

        //#####Wait until case is replicated to DataBase
        Thread.sleep(2000);

         assertThat(DataBaseConnector.getDrawDownCase(DataBaseConnector.getCreditApplication(RegisteredUser.customerId)),is(not(emptyString())));
        Log.info("DrawDown case ID: " + DataBaseConnector.getDrawDownCase(DataBaseConnector.getCreditApplication(RegisteredUser.customerId)));

    }

    @And("^Slider is hidden$")
    public void sliderIsHidden() throws Throwable {
        Assert.assertFalse(facade.selfServ().sliderIsShown(), "Slider is still shown");

    }

    /*
    @And("^Close Draw case$")
    public void closeDrawDownCase () throws Throwable{
        try {
            DataBaseConnector.rejectCase(DataBaseConnector.getDrawDownCase(DataBaseConnector.getCreditApplication(RegisteredUser.customerId)));
        }
        catch (Throwable th){
            Log.info("No DrawDown cases to close");
        }
    }
    */


    @Then("^Extra services are disabled$")
    public void extraServicesAreDisabled() throws Throwable {
        Assert.assertFalse(facade.selfServ().changeDueDateIsAvailable(), "Change DUE DATE button is ACTIVE");
        Assert.assertFalse(facade.selfServ().paymentHolidayIsAvailable(), "PH button is ACTIVE");
        Assert.assertFalse(facade.selfServ().upgradeIsAvailable(), "Upgrade button is ACTIVE");

    }

    @And("^User changes password$")
    public void userChangesPassword() throws Throwable {
       userOpensDetailsForm();
       facade.selfServ().enterCurrentPassword(RegisteredUser.password);
       facade.selfServ().setNewPassword("Qwerty1234");
       RegisteredUser.password="Qwerty1234";
       facade.selfServ().confirmChanges();


    }

    @And("^User Log out$")
    public void userLogOut() throws Throwable {
        facade.selfServ().logOut();
    }

    @Then("^User is able to log in with new password$")
    public void userIsAbleToLogInWithNewPassword() throws Throwable {
        userLogInWithCustomerIdAndPassword(RegisteredUser.customerId,RegisteredUser.password);
        userOpensPersonalDetails();
        userOpensDetailsForm();
        facade.selfServ().enterCurrentPassword(RegisteredUser.password);
        facade.selfServ().setNewPassword("Qwerty123");
        facade.selfServ().confirmChanges();
    }

    @Then("^Reject draw case$")
    public void rejectDrawCase() throws Throwable {
        String caseId= DataBaseConnector.getDrawDownCase(DataBaseConnector.getCreditApplication(RegisteredUser.customerId));
        Log.info("Rejecting draw case : ".concat(caseId).concat("..."));
        Case.rejectCase(caseId);
    }

    @And("^New application button is shown$")
    public void newApplicationButtonIsShown() throws Throwable {
        Assert.assertTrue(facade.selfServ().newApplicationButtonIsShown(),"New application button is shown");
    }

    @When("^User clicks new application button$")
    public void userClicksNewApplicationButton() throws Throwable {
        facade.selfServ().applyForProduct();
    }

    @And("^User opens invoices tab$")
    public void userOpensInvoicesTab() throws Throwable {
            facade.selfServ().openInvoicesTab();
            Assert.assertTrue(facade.selfServ().invoiceTabIsOpened());
    }

    @When("^User clicks Upgrade button$")
    public void userClicksUpgradeButton() throws Throwable {
        facade.selfServ().upgradeCreditLine();
    }

    @Then("^POP up with available products is shown$")
    public void popUpWithAvailableProductsIsShown() throws Throwable {
        Assert.assertTrue(facade.selfServ().upgradePopUpIsShown(),"Upgrade POP UP  displaying");
    }

    @When("^User selects product$")
    public void userSelectsProduct() throws Throwable {
        facade.selfServ().selectNewProductLimit();
        facade.selfServ().submitProductChange();

    }

    @And("^User changes living address$")
    public void userChangesLivingAddress() throws Throwable {
        userOpensPersonalDetails();
        userOpensDetailsForm();
        facade.selfServ().changeAddress();
        facade.selfServ().confirmChanges();
    }


    @Then("^Address is changed$")
    public void addressIsChanged() throws Throwable {
        Thread.sleep(1500);
        Assert.assertEquals(RegisteredUser.address,facade.selfServ().getAddressShownInSelfService());


    }

    @When("^User clicks Upsell button$")
    public void userClicksUpsellButton() throws Throwable {
        facade.selfServ().upsell();
    }
}

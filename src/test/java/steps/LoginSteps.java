package steps;

import Base.BaseUtils;
import Facade.Facade;
import Recorders.Log;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import users.RegisteredUser;

import java.security.PublicKey;
import java.util.concurrent.TimeoutException;

/**
 * Created by vladyslav on 7/28/17.
 */
public class LoginSteps extends BaseUtils {


    BaseUtils base;


    public LoginSteps(BaseUtils base) {

       // super(base);

        this.base=base;

    }




    @Given("^User opens main page$")
    public void userOpensMainPage() throws Throwable {
        facade.mainPage().closeTipsWindow();
        facade.mainPage().closeCookie();

    }

    @When("^User clicks login$")
    public void userClicksLogin() throws Throwable {
        facade.mainPage().openLoginForm();
    }


    @Then("^Log in form is shown$")
    public void logInFormIsShown() throws Throwable {
       Assert.assertTrue(facade.mainPage().logInWindowIsDisplayed());
    }


    @And("^Clicks submit$")
    public void clicksSubmit() throws Throwable {
        facade.mainPage().submitLogIn();
    }

    @Then("^Self service page is shown$")
    public void selfServicePageIsShown() throws Throwable {
        Assert.assertTrue(facade.selfServ().selfServicePageIsShown(),"Self service page is NOT displayed");
    }


    @And("^User enters login as ([^\\\"]*) and password as ([^\\\"]*)$")
    public void userEntersLoginAsLoginAndPasswordAsOtp(String login, String otp) throws Throwable {

        facade.mainPage().setLogin(login);
        facade.mainPage().setPassword(otp);
        facade.mainPage().submitLogIn();
    }


    @When("^User requests temporary password for ([^\\\"]*)$")
    public void userRequestsTemporaryPasswordForLogin(String login) throws Throwable {
        facade.mainPage().openLoginForm();
        facade.mainPage().openPassRecoverWindow();
        facade.mainPage().requestPassRecovery(login);

    }

    @Then("^New password window is shown$")
    public void newPasswordWindowIsShown() throws Throwable {
        try {
            Assert.assertTrue(facade.mainPage().isPopUpDisplayed(),"NO SET NEW PASS WINDOW");
        }
        catch (AssertionError ae){
            Log.error("New Password window IS NOT shown");
            throw new AssertionError("New Password window IS NOT shown");
        }
    }

    @When("^User sets new password as ([^\\\"]*)$")
    public void userSetsNewPasswordAsNewPass(String newPass) throws Throwable {
        facade.mainPage().submitNewPass(newPass);

    }

    


    @And("^User login to application with ([^\\\"]*) and ([^\\\"]*)$")
    public void userLoginToApplicationWithPhoneandPassword(String phone, String password) throws Throwable {
        facade.mainPage().openLoginForm();
        String prefix="+3706";
        String phoneLong=prefix.concat(phone);
        facade.mainPage().setLogin(phoneLong);
        facade.mainPage().setPassword(password);
        facade.mainPage().submitLogIn();
    }




    @When("^User login to application with login ([^\\\"]*) and password as \"([^\"]*)\"$")
    public void userLoginToApplicationWithLoginLoginAndPasswordAs(String login,String password) throws Throwable {
        RegisteredUser.customerId=login;
        facade.mainPage().openLoginForm();
        facade.mainPage().setLogin(login);
        facade.mainPage().setPassword(password);
        facade.mainPage().submitLogIn();
    }



    @Then("^Wrong credentials POP Up is shown$")
    public void wrongCredentialsPOPUpIsShown() throws Throwable {
                Assert.assertEquals(facade.mainPage().popUpHeader(),"Neteisingi prisijungimo duomenys");

    }

}

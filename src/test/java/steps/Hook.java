package steps;

import Base.BaseUtils;
import Base.Environment;
import Facade.Facade;
import Recorders.Log;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import database.DataBaseConnector;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


/**
 * Created by vladyslav on 7/22/17.
 */
public class Hook extends BaseUtils {

    @Before
    public void setUp(Scenario scenario)throws Throwable{
        Log.startFeature(scenario.getName());
        startBrowser();
        openCredit24("uat");
        DataBaseConnector.setDataBaseConnection();
        //VideoRecorder.startRecording(base.driver);

    }


   @After
    public void tearDown (Scenario scenario)throws Throwable{
        System.out.println(" ");
        System.out.println(scenario.getName() + " " + scenario.getStatus().toUpperCase());
        Log.endFeature(scenario.getStatus().toUpperCase());
        if (driver!=null) {
            driver.close();
        }
   //    VideoRecorder.stopRecording(scenario.getName(),scenario);

        if (DataBaseConnector.con !=null){
            DataBaseConnector.closeDatabaseConnection();
        }


   }

}

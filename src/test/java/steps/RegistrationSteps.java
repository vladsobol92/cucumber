package steps;

import Base.BaseUtils;
import Recorders.Log;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import database.DataBaseConnector;
import org.testng.Assert;
import pages.constants.Bank;
import pages.externalResources.DataGeneratorPage;
import users.RegisteredUser;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by vladyslav on 7/29/17.
 */
public class RegistrationSteps extends BaseUtils {


    BaseUtils base;

    public RegistrationSteps(BaseUtils base) {
       // super(base);
        this.base=base;
    }

/*

  @Before
    public void getDataFromDataGenerator(){
        base.driver=new ChromeDriver();
        Facade facade = new Facade(base.driver);
        facade.dataGen().setUserData();

    }

 */




    @When("^User opens registration form$")
    public void userOpensRegistrationForm() throws Throwable {
        facade.mainPage().openLoginForm();
        facade.mainPage().openApplication();
        Assert.assertTrue(facade.registration().registrationPageDisplayed(),"Registration form is displayed");

    }

    @And("^User registers with idCode \"([^\"]*)\"$")
    public void userRegistersWithIdCode(String idCode) throws Throwable {


        facade.registration().setIdCode(idCode);
        facade.registration().setDefaultPassword();
        facade.registration().setBankAccount(Bank.SWEDBANK);
        facade.registration().setRandomPhone();
        facade.registration().requestOtp();
        Assert.assertFalse(facade.registration().isUserRegistered(),"POP UP shows that user is already registered");
        facade.registration().closeOtpPopUp();
        facade.registration().setOtp();
       // facade.registration().setRandomEmail();
        facade.registration().setMail("vladislav.sobol@ipfdigital.com");

        facade.registration().setPolitician(false);
        facade.registration().acceptTerms();
        facade.registration().submitApplication();

    }




    @And("^User is registered in the system$")
    public void userIsRegisteredInTheSystem() throws Throwable {

        Log.info("User is registered");
        Log.info("The customerID is: " + DataBaseConnector.getUserId(RegisteredUser.idCode));
        RegisteredUser.customerId= DataBaseConnector.getUserId(RegisteredUser.idCode);
        assertThat(DataBaseConnector.getUserId(RegisteredUser.idCode),is(not(emptyString())));

    }



    @Then("^Bank verification page is shown$")
    public void bankVerificationPageIsShown() throws AssertionError {

            Assert.assertTrue(facade.bankPage().bankVerificationPageDisplayed(),"Bank verification page  should be shown");


    }


    @When("^User enters ssn \"([^\"]*)\"$")
    public void userEntersSsn(String idCode) throws Throwable {
        facade.registration().setIdCode(idCode);
        facade.registration().setDefaultPassword();
        facade.registration().setBankAccount(Bank.SWEDBANK);
    }

    @And("^User requests OTP$")
    public void userRequestsOTP() throws Throwable {
        facade.registration().setRandomPhone();
        facade.registration().requestOtp();

    }

    @Then("^The pop up shows that user is already registered$")
    public void thePopUpShowsThatUserIsAlreadyRegistered() throws Throwable {
        Assert.assertTrue(facade.registration().isUserRegistered(),"POP UP says that user is already registered");
    }



    @And("^User registers with SSN from external resource$")
    public void userRegistersWithSSN() throws Throwable {

        userOpensRegistrationForm();

        //facade.registration().setIdCode(RegisteredUser.idCode);
        facade.registration().setIdCode(DataGeneratorPage.externalUserData.get(0));
        facade.registration().setDefaultPassword();
        facade.registration().setBankAccount(Bank.SWEDBANK);
        facade.registration().setRandomPhone();
        facade.registration().requestOtp();
        Assert.assertFalse(facade.registration().isUserRegistered(),"POP UP does not show that user is already registered");
        facade.registration().closeOtpPopUp();
        facade.registration().setOtp();
         facade.registration().setRandomEmail();
       // facade.registration().setMail("vladislav.sobol@ipfdigital.com");
        facade.registration().setPolitician(false);
        facade.registration().acceptTerms();
        facade.registration().submitApplication();
        bankVerificationPageIsShown();
        userIsRegisteredInTheSystem();

    }

    @And("^User registers with SSN from external resource and Bank ([^\\\"]*)$")
    public void userRegistersWithSSNFromExternalResourceAndBankBank(Bank bank) throws Throwable {
        userOpensRegistrationForm();

        //facade.registration().setIdCode(RegisteredUser.idCode);
        facade.registration().setIdCode(DataGeneratorPage.externalUserData.get(0));
        facade.registration().setDefaultPassword();
        facade.registration().setBankAccount(bank);
        facade.registration().setRandomPhone();
        facade.registration().requestOtp();
        Assert.assertFalse(facade.registration().isUserRegistered(),"POP UP does not show that user is already registered");
        facade.registration().closeOtpPopUp();
        facade.registration().setOtp();
        facade.registration().setRandomEmail();
        // facade.registration().setMail("vladislav.sobol@ipfdigital.com");
        facade.registration().setPolitician(false);
        facade.registration().acceptTerms();
        facade.registration().submitApplication();
        bankVerificationPageIsShown();
        userIsRegisteredInTheSystem();

    }
}

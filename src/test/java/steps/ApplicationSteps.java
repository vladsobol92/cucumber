package steps;

import Base.BaseUtils;
import Recorders.Log;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import cucumber.api.java.en.When;
import database.DataBaseConnector;
import org.testng.Assert;
import pages.constants.*;
import pages.externalResources.DataGeneratorPage;
import soap.Case;
import users.RegisteredUser;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by vladyslav on 8/4/17.
 */
public class ApplicationSteps extends BaseUtils {

    BaseUtils base;

    public ApplicationSteps(BaseUtils base) throws Throwable {
        this.base=base;
    }



    @Then("^User application is submitted$")
    public void userApplicationIsSubmitted() throws Throwable {
        facade.thankYouPage().finishApplication();

        String cac = DataBaseConnector.getCreditApplicationCase(DataBaseConnector.getCreditApplication(DataBaseConnector.getUserId(RegisteredUser.idCode)));
        Log.info("Credit application ID :" + DataBaseConnector.getCreditApplication(DataBaseConnector.getUserId(RegisteredUser.idCode)));
        Log.info("Credit application case ID: " + cac);
        assertThat(cac,is(not(emptyString())));



    }


    @And("^User applies for CL product with principal (\\d+) and draw (\\d+)$")
    public void userAppliesForCLProductWithDraw(int principal, int draw) throws Throwable {

        Assert.assertTrue(facade.productsPage().productSelectionPageIsDisplayed());
       Assert.assertTrue(facade.creditLinepage().creditLineSelectionIsDisplayed());
        facade.creditLinepage().setLimit(principal);
        facade.creditLinepage().setAmountToDraw(draw);
        facade.productsPage().submit();
        Assert.assertTrue(facade.legalPage().LegalPageIsShown());
        facade.legalPage().acceptLegalTerms();
        facade.legalPage().submit();
    }

    @When("^User applies for IL with principal (\\d+) and maturity (\\d+)$")
    public void userAppliesForILWithPrincipalAndMaturity(int principal, int maturity) throws Throwable {
        Assert.assertTrue(facade.productsPage().productSelectionPageIsDisplayed());
        facade.productsPage().selectInstallment();
        facade.installment().setAmountToDraw(principal);
        facade.installment().setMaturityPeriod(maturity);
        facade.installment().submit();
        Assert.assertTrue(facade.legalPage().LegalPageIsShown());
        facade.legalPage().acceptLegalTerms();
        facade.legalPage().submit();
    }

   /*
   ........................
    User Registers in the system with SSN
    .........................
    */
    @Given("^User registers in the system with ssn \"([^\"]*)\"$")
    public void userRegistersInTheSystemWithSsn(String idCode) throws Throwable {
        //User opens registration form

        RegistrationSteps reg = new RegistrationSteps(base);
        reg.userOpensRegistrationForm();
        reg.userRegistersWithIdCode(idCode);

        //User is registered and Bank page is shown
        reg.bankVerificationPageIsShown();
        reg.userIsRegisteredInTheSystem();

        //Make 1c verification
        UserMakes1cVerification();

    }

    @And("^User makes 1c verification$")
    public void UserMakes1cVerification() throws Throwable {
        facade.bankPage().confirmBank();
        DataBaseConnector.verifyBankAccount(RegisteredUser.idCode);
        facade.bankPage().refreshPage();
    }


    @When("^User provides general data \"([^\"]*)\"$")
    public void userProvidesgeneralData(String bankNumber) throws Throwable {

            facade.generalInfo().setEducation(Education.UNIVERSITY);
            facade.generalInfo().setResidence(Residence.OWNERAPARTMENT);
            facade.generalInfo().setBankNumber(bankNumber);
            facade.generalInfo().acceptTerms();
            facade.generalInfo().submit();

    }



    @And("^User provides financial data$")
    public void userProvidesfinancialData() throws Throwable {
        Assert.assertTrue( facade.financePage().financialPageIsDisplayed());

            facade.financePage().setEmploymentType(Employment.FULLTIME);
            facade.financePage().setIncomePeriodEnums(EmploymentDuration.MORETHENYEAR);
            facade.financePage().setIncome(3000);
            facade.financePage().setMonthlyObligations(500);
            facade.financePage().submit();


    }


    @When("^User applies for product$")
    public void userAppliesForProduct() throws Throwable {

        facade.generalInfo().setEducation(Education.UNIVERSITY);
        facade.generalInfo().setResidence(Residence.OWNERAPARTMENT);
        facade.generalInfo().acceptTerms();
        facade.generalInfo().submit();
        userProvidesfinancialData();
        userAppliesForILWithPrincipalAndMaturity(1000,20);
        facade.thankYouPage().finishApplication();
    }

    @Then("^Customer care rejects application case$")
    public void customerCareRejectsApplicationCase() throws Throwable {
        String creditApplicationCase = DataBaseConnector.getCreditApplicationCase(DataBaseConnector.getCreditApplication(RegisteredUser.customerId));
        Log.info("Credit application case ID: " + creditApplicationCase);
        Case.rejectCase(creditApplicationCase);
        Log.info("Credit Application case: ".concat(creditApplicationCase).concat(" is REJECTED"));

    }

    @Then("^General data page is shown$")
    public void generalDataPageIsShown() throws Throwable {
        Assert.assertTrue(facade.generalInfo().generalInfoPageDisplayed());
    }

    @When("^Application BKA response is DONE$")
    public void applicationBKAResponseIsDone() throws Throwable {


        int timer=0;

        String status= DataBaseConnector.getBKAresponse(DataBaseConnector.getCreditApplication(RegisteredUser.customerId));
        while (!status.equals("DONE")){
            Thread.sleep(5000);
            status= DataBaseConnector.getBKAresponse(DataBaseConnector.getCreditApplication(RegisteredUser.customerId));
            System.out.println(status);
            System.out.println("Time is: "+timer);
            timer=timer+5;
            if(timer>750){
                break;
            }
        }
        Log.info("BKA status is: " + DataBaseConnector.getBKAresponse(DataBaseConnector.getCreditApplication(RegisteredUser.customerId)));

    }

    @Then("^Customer care accepts application case$")
    public void customerCareAcceptsApplicationCase() throws Throwable {
        String caseId= DataBaseConnector.getCreditApplicationCase(DataBaseConnector.getCreditApplication(RegisteredUser.customerId));
        Log.info("Closing application case: ".concat(caseId).concat("..."));
        Case.acceptCase(caseId);

    }

    @And("^Customer care accepts draw case$")
    public void customerCareAcceptsDrawCase() throws Throwable {
        String caseId= DataBaseConnector.getDrawDownCase(DataBaseConnector.getCreditApplication(RegisteredUser.customerId));
        Log.info("Closing draw case case: ".concat(caseId).concat("..."));
        Case.acceptCase(caseId);
    }

    @And("^Contract is created$")
    public void contractIsCreated() throws Throwable {
        Thread.sleep(2000);
        String contractId= DataBaseConnector.getContractId(RegisteredUser.customerId);
        assertThat(contractId,is(not(emptyString())));
        Log.info("Contract ID: "+contractId);
    }


    @When("^User provides general data from external resource$")
    public void userProvidesGeneralDataFromExternalResource() throws Throwable {
        facade.generalInfo().setEducation(Education.UNIVERSITY);
        facade.generalInfo().setResidence(Residence.OWNERAPARTMENT);
        facade.generalInfo().setBankNumber(DataGeneratorPage.externalUserData.get(6));
        facade.generalInfo().setAddress("HardCodedAddress 1");
        facade.generalInfo().acceptTerms();
        facade.generalInfo().submit();
    }


    @When("^User submits general data$")
    public void userSubmitsGeneralData() throws Throwable {

        facade.generalInfo().acceptTerms();
        facade.generalInfo().submit();
    }

    @Then("^Financial data page is shown$")
    public void financialDataPageIsShown() throws Throwable {
        Assert.assertTrue( facade.financePage().financialPageIsDisplayed());
    }

    @When("^User submits data$")
    public void userSubmitsData() throws Throwable {

        facade.generalInfo().submit();
    }

    @Then("^CL selection page is shown$")
    public void productSelectionPageIsShown() throws Throwable {
        Assert.assertTrue(facade.productsPage().productSelectionPageIsDisplayed());
        Assert.assertTrue(facade.creditLinepage().creditLineSelectionIsDisplayed());
    }

    @Then("^IL selection page is shown$")
    public void ilSelectionPageIsShown() throws Throwable {
        Assert.assertTrue(facade.productsPage().productSelectionPageIsDisplayed());
        Assert.assertTrue(facade.installment().installmentSelectionIsDisplayed());
    }
}

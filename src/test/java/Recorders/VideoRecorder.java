package Recorders;

import cucumber.api.Scenario;
import org.monte.media.Format;
import org.monte.media.FormatKeys;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;

import java.awt.*;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.monte.media.FormatKeys.*;
import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.VideoFormatKeys.*;

/**
 * Created by vladyslav on 9/9/17.
 */
public class VideoRecorder {

    /*
    ++++++++++++++++++++++++++
    This is a part for recording Videos
    ++++++++++++++++++++++++++
    */

    private final static String RECORD_DIRECTORY = "/Users/vladyslav/IdeaProjects/cucumber/src/video/";

    private static ScreenRecorder screenRecorder;

    public static void startRecording(WebDriver driver) {

        try {
            GraphicsConfiguration gc = GraphicsEnvironment
                    .getLocalGraphicsEnvironment().getDefaultScreenDevice()
                    .getDefaultConfiguration();

            File dir = new File(RECORD_DIRECTORY);

            // Record only browsers window
            // to decrease the size of the file
            org.openqa.selenium.Point point = driver.manage().window().getPosition();
            org.openqa.selenium.Dimension dimension = driver.manage().window().getSize();

            Rectangle rectangle = new Rectangle(point.x, point.y,
                    dimension.width, dimension.height);

            screenRecorder = new ScreenRecorder(gc, rectangle,
                    new Format(MediaTypeKey, FormatKeys.MediaType.FILE, MimeTypeKey,
                            MIME_AVI),
                    new Format(MediaTypeKey, FormatKeys.MediaType.VIDEO, EncodingKey,
                            ENCODING_AVI_MJPG,
                            CompressorNameKey,
                            ENCODING_AVI_MJPG, DepthKey,
                            24, FrameRateKey, Rational.valueOf(12), QualityKey,
                            1.0f, KeyFrameIntervalKey, 15 * 60), new Format(
                    MediaTypeKey, MediaType.VIDEO, EncodingKey,
                    "black", FrameRateKey, Rational.valueOf(12)), null,
                    dir);

            screenRecorder.start();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void stopRecording(String recordName, Scenario scenario) {

        /*
        Rename newly recorded video
        We save video only for the FAILED scenario.
        We record all of the tests, but if result is not FAILURE the file will be deleted
        */
        try {
            screenRecorder.stop();

            // Rename newly created file .avi file,
            if (recordName != null && scenario.isFailed() ) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "yyyy-MM-dd HH.mm.ss");
                File newFileName = new File(String.format("%s%s %s.avi",
                        RECORD_DIRECTORY,recordName,
                        dateFormat.format(new Date())));

                screenRecorder.getCreatedMovieFiles().get(0)
                        .renameTo(newFileName);
            }

            else {

               screenRecorder.getCreatedMovieFiles().get(0)
                        .delete();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }




}

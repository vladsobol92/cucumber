package users;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by vladyslav on 7/29/17.
 */
public class RegisteredUser {
   public static String idCode;
   public static String phone;
   public static String password;
   public static String bank;
   public static String customerId;
   public static String email;
   public static String address;


    public static void setIdCode (String idCode){
        RegisteredUser.idCode=idCode;
    }

    public static void setPassword (String password){
        RegisteredUser.password=password;

    }


    public static void setBankAccount (String bank){
        RegisteredUser.bank=bank;

    }

    public static void setPhone (String phone){
        RegisteredUser.phone=phone;

    }

    public static void setEmail(String email) {
        RegisteredUser.email = email;
    }


    public static String getIdCode() {
        return idCode;
    }

    public static String getPhone() {
        return phone;
    }

    public static String getEmail(){
        return email;
    }

    public static String getPassword() {
        return password;
    }

    public static String getBank() {
        return bank;
    }






    public static List <String> getSelfServiceData (){

        List <String> data = new ArrayList<String>();
        data.add(getPhone());
        data.add(getEmail());
        data.add(getBank());

        return data;

    }
}

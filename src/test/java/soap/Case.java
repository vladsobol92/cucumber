package soap;

import Base.Environment;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by vladyslav on 10/17/17.
 */
public class Case {

   static String service;



    public static void acceptCase(String caseId){



        service="CaseService";


        try {

            URL url = new URL(Environment.soapEndpoint.concat(service));

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/xml");

            OutputStream os = connection.getOutputStream();


            String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"\n" +
                    "                  xmlns:com=\"http://www.sving.com/schemas/common\" xmlns:case=\"http://www.sving.com/schemas/case\">\n" +
                    "    <soapenv:Header>\n" +
                    "        <com:sfRequest country=\"LT\" user=\"?\"/>\n" +
                    "    </soapenv:Header>\n" +
                    "    <soapenv:Body>\n" +
                    "        <case:closeCaseRequest>\n" +
                    "            <case:id>"+caseId+"</case:id>\n" +
                    "            <case:result>Accepted</case:result>\n" +
                    "            <!--Optional:-->\n" +
                    "            <case:parameters>\n" +
                    "                <!--Zero or more repetitions:-->\n" +
                    "                <com:item>\n" +
                    "                    <com:key></com:key>\n" +
                    "                    <com:value></com:value>\n" +
                    "                </com:item>\n" +
                    "            </case:parameters>\n" +
                    "        </case:closeCaseRequest>\n" +
                    "    </soapenv:Body>\n" +
                    "</soapenv:Envelope>";


            os.write(request.getBytes());

            connection.getResponseCode();
            connection.disconnect();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }


    }


    public static void rejectCase (String caseId){
        service="CaseService";


        try {

            URL url = new URL(Environment.soapEndpoint.concat(service));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/xml");

            OutputStream os = connection.getOutputStream();


            String request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"\n" +
                    "                  xmlns:com=\"http://www.sving.com/schemas/common\" xmlns:case=\"http://www.sving.com/schemas/case\">\n" +
                    "    <soapenv:Header>\n" +
                    "        <com:sfRequest country=\"LT\" user=\"?\"/>\n" +
                    "    </soapenv:Header>\n" +
                    "    <soapenv:Body>\n" +
                    "        <case:closeCaseRequest>\n" +
                    "            <case:id>"+caseId+"</case:id>\n" +
                    "            <case:result>Rejected</case:result>\n" +
                    "            <!--Optional:-->\n" +
                    "            <case:parameters>\n" +
                    "                <!--Zero or more repetitions:-->\n" +
                    "                <com:item>\n" +
                    "                    <com:key></com:key>\n" +
                    "                    <com:value></com:value>\n" +
                    "                </com:item>\n" +
                    "            </case:parameters>\n" +
                    "        </case:closeCaseRequest>\n" +
                    "    </soapenv:Body>\n" +
                    "</soapenv:Envelope>";


            os.write(request.getBytes());

            connection.getResponseCode();
            connection.disconnect();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }




}





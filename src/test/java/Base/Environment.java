package Base;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

/**
 * Created by vladyslav on 10/13/17.
 */
public class Environment {

/*

    public static final String environmentUrl="https://st01.credit24.com/lt";
    public static final String databaseUrl="jdbc:mysql://mysqlrds1.cp4dooyqpnvv.eu-west-1.rds.amazonaws.com/alpha_aio";
    public static final String databaseUsername="v.sobol";
    public static final String databasePassword="84smo0s-k(c^b-)l%5";
    public static final String soapEndpoint="http://172.0.1.35:8080/admin/ws/";


*/
/*

    public static final String databaseUrl= "jdbc:mysql://aiouat-rds.aiouat.ipfdigital.io/loanengine0003";
    public static final String databaseUsername="testteam";
    public static final String databasePassword="7Pq5vralVpP77TPU";
    public static final String environmentUrl="https://uat.credit24.com/lt";
    public static final String soapEndpoint="https://uat.credit24.com/admin/ws/" ;

*/


    public static   String databaseUrl;
    public static  String databaseUsername;
    public static  String databasePassword;
    public  static String environmentUrl;
    public  static String soapEndpoint;


    public static void  setEnvironment(String environment) {

        JSONParser parser = new JSONParser();


        try {

            Object obj = parser.parse(new FileReader("/Users/vladyslav/IdeaProjects/cucumber/data"));

            JSONObject fileData = (JSONObject) obj;

            JSONObject environments = (JSONObject) fileData.get("environment");

            JSONObject specificEnvironment = (JSONObject) environments.get(environment);
            databaseUrl = (String)specificEnvironment.get("databaseUrl");
            databaseUsername = (String)specificEnvironment.get("databaseUsername");
            databasePassword = (String) specificEnvironment.get("databasePassword");
            environmentUrl = (String) specificEnvironment.get("environmentUrl");
            soapEndpoint = (String) specificEnvironment.get("soapEndpoint");
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }




}

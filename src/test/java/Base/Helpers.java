package Base;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by vladyslav on 8/5/17.
 */


public class Helpers {

    public WebDriver driver;
    public WebDriverWait wait;




    public WebElement waitToBeShown (By locator) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

    }

    public List <WebElement> waitIsShown (By locator){
      return   wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public WebElement interactWith(By locator){

        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        try{
            driver.findElement(By.id("page-blocker"));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("page-blocker")));
        }
        catch (NoSuchElementException noel){

        }

        ScrollToElement(locator);

        wait.until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);



    }

    public void scroll(int value){
        String scrolling = ( "scroll(0," + value + ")" );
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript(scrolling,"");
    }




    public WebElement ScrollToElement(By locator) {
        WebElement element = waitToBeShown(locator);

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(" + element.getLocation().x + "," + (element.getLocation().y
                - 300) + ");");

        return element;
    }


    public void refreshPage(){
        driver.get(driver.getCurrentUrl());
        waitToBeShown(By.tagName("body"));
    }



}

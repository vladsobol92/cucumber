package Base;

import Facade.Facade;
import database.DataBaseConnector;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by vladyslav on 7/22/17.
 */
public class BaseUtils {

    public  WebDriver driver;
    public  WebDriverWait wait;
    public static  Facade facade;




    public void startBrowser(){

        if(this.driver==null){
            this.driver=new ChromeDriver();

        }

    }

    public void startBrowser(String parameter){
        ChromeOptions options = new ChromeOptions();
        options.addArguments(parameter);

        if(this.driver==null){
            this.driver=new ChromeDriver(options);
        }

    }


    public void openCredit24(String environment){
        Environment.setEnvironment(environment);
        facade = new Facade(driver);
        facade.dataGen().setUserData();
        driver.get(Environment.environmentUrl);
//        facade.mainPage().closeTipsWindow();
        facade.mainPage().closeCookie();
    }


}

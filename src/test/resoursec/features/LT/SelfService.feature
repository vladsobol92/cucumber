Feature: SelfService functionality

  @positive @smoke
  Scenario: Personal data update
    Given User log in with customerId "2764599" and password "Qwerty123"
    When User opens personal details
    And User changes email
    And User changes phone
    And User changes bankNumber
    Then Data is changed

  @positive @smoke
  Scenario: Living address update
    Given User log in with customerId "2764599" and password "Qwerty123"
    When User opens personal details
    And User changes living address
    Then Address is changed


  @positive @smoke
  Scenario: Draw money
    Given User log in with customerId "2764599" and password "Qwerty123"
    When User requests draw 50
    Then New drawdown case is opened
    And Slider is hidden
    Then Reject draw case


  @negative @smoke
  Scenario: Extra services blocking due to late invoice
    Given User log in with customerId "2764599" and password "Qwerty123"
    When Self service page is shown
    Then Extra services are disabled

  @positive @smoke
  Scenario: Password change
    Given User log in with customerId "2764599" and password "Qwerty123"
    When Self service page is shown
    Then User opens personal details
    And User changes password
    And User Log out
    Then User is able to log in with new password

  @smoke
  Scenario: Self service displaying for customer with paid contract
    Given User log in with customerId "2783868" and password "Qwerty123"
    Then Self service page is shown
    And New application button is shown
    When User clicks new application button
    Then General data page is shown

  @smoke
  Scenario: No draw slider for IL customer
    Given User log in with customerId "2764596" and password "Qwerty123"
    Then Slider is hidden


  @smoke
  Scenario: Switching between tabs
    Given User log in with customerId "2764595" and password "Qwerty123"
    When Self service page is shown
    Then User opens personal details
    And User opens invoices tab


  @smoke @extraService
  Scenario: CL Upgrade product
    Given User log in with customerId "2719615" and password "Qwerty123"
    When User clicks Upgrade button
    Then POP up with available products is shown
    When User selects product
    Then General data page is shown
    When User submits general data
    Then Financial data page is shown
    When User submits data
    Then CL selection page is shown


  @smoke @extraService
  Scenario: IL Upsell product
    Given User log in with customerId "2719623" and password "Qwerty123"
    When User clicks Upsell button
    Then General data page is shown
    When User submits general data
    Then Financial data page is shown
    When User submits data
    Then IL selection page is shown






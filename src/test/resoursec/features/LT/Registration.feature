Feature: User registration in the system

    @positive
    Scenario: User registers in the system
      Given User opens registration form
      And User registers with idCode "37805302916"
      Then Bank verification page is shown
      And User is registered in the system

    @negative
    Scenario: Registration with already used SSN
      Given User opens registration form
      When User enters ssn "37805304061"
      And User requests OTP
      Then The pop up shows that user is already registered

     @externalData
    Scenario: External resources ID code
      Given User opens registration form
      And User registers with SSN from external resource
      Then Bank verification page is shown
      And User is registered in the system


  @externalData
  Scenario Outline: Registration with different bank accounts
    Given User opens registration form
    And User registers with SSN from external resource and Bank <bank>
    Then Bank verification page is shown
    And User is registered in the system
    Examples:
      |bank    |
      |SWEDBANK|
      |SEB|
      |DNB|
      |NORDEA|
      |CITADELE|
      |CIAULIU|


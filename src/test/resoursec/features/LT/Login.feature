Feature: Login functionality of Credit24

  @positive @smoke
  Scenario Outline: User Log In to Credit24 LT with different types of login
    Given User login to application with login <login> and password as "Qwerty123"
    Then Self service page is shown
    Examples:
    |login  |
    |2764595|
    |2764596|
    |2718957|


  @positive @smoke
  Scenario Outline: User request password recovery with different types of login
    Given User requests temporary password for <login>
    And User enters login as <login> and password as <otp>
    Then New password window is shown
    When User sets new password as <newPass>
    Then Self service page is shown
    Examples:
      |login        | otp      |newPass   |
      |+37069026578 |12345678  |Qwerty123 |
      |2718959      |12345678  |Qwerty123 |

  
    
    
   @negative @smoke
   Scenario Outline: Warning message showing after log in with wrong credentials
    When User login to application with login <login> and password as "Qwerty123454545"
    Then Wrong credentials POP Up is shown
    Examples:
     |login   |
     |2718957 |




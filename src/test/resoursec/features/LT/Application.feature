Feature: Application


      @automate
      Scenario: CL application full
        Given User registers with SSN from external resource
        Then User makes 1c verification
        When User provides general data from external resource
        And User provides financial data
        And User applies for CL product with principal 1000 and draw 500
        Then User application is submitted
        When Application BKA response is DONE
        Then Customer care accepts application case
        And Contract is created
        When New drawdown case is opened
        Then Customer care accepts draw case

      @automate
      Scenario: IL application full
        Given User registers with SSN from external resource
        Then User makes 1c verification
        When User provides general data from external resource
        And User provides financial data
        And User applies for IL with principal 1000 and maturity 24
        Then User application is submitted
        When Application BKA response is DONE
        Then Customer care accepts application case
        When New drawdown case is opened
        Then Customer care accepts draw case
        And Contract is created


      @smoke
      Scenario: CL application
        Given User registers with SSN from external resource
        Then User makes 1c verification
        When User provides general data from external resource
        And User provides financial data
        And User applies for CL product with principal 500 and draw 300
        Then User application is submitted

      @smoke
      Scenario: CL application without draw
        Given User registers with SSN from external resource
        Then User makes 1c verification
        When User provides general data from external resource
        And User provides financial data
        And User applies for CL product with principal 750 and draw 0
        Then User application is submitted


      @smoke
      Scenario: IL application
        Given User registers with SSN from external resource
        Then User makes 1c verification
        When User provides general data from external resource
        And User provides financial data
        And User applies for IL with principal 500 and maturity 24
        Then User application is submitted


      @full @cl
      Scenario: Full user application process CL
        Given  User registers in the system with ssn "37006300119"
        When User provides general data "LT341111100000000187"
        And User provides financial data
        And User applies for CL product with principal 1000 and draw 200
        Then User application is submitted
        When Application BKA response is DONE
        Then Customer care accepts application case
        And Contract is created
        When New drawdown case is opened
        Then Customer care accepts draw case



      @full @il
      Scenario: Full User application process IL
        Given  User registers in the system with ssn "37006300032"
        When User provides general data "LT881111100000000185"
        And User provides financial data
        And User applies for IL with principal 1000 and maturity 24
        Then User application is submitted
        When Application BKA response is DONE
        Then Customer care accepts application case
        When New drawdown case is opened
        Then Customer care accepts draw case
        And Contract is created



      @positive
      Scenario: User applies for CL
        Given  User registers in the system with ssn "37805304769"
        When User provides general data "LT561111100000000179"
        And User provides financial data
        And User applies for CL product with principal 750 and draw 500
        Then User application is submitted


       @positive
      Scenario: User applies for IL
        Given  User registers in the system with ssn "37805304192"
        When User provides general data "LT491111100000000111"
        And User provides financial data
        And User applies for IL with principal 1000 and maturity 24
        Then User application is submitted




      @smoke @negative
      Scenario: Reject User application
        Given User login to application with login 2735563 and password as "Qwerty123"
        When User applies for product
        Then Customer care rejects application case
        When User login to application with login 2735563 and password as "Qwerty123"
        Then General data page is shown



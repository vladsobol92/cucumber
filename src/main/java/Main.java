import org.apache.xpath.SourceTree;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import java.io.File;
import java.io.FileReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.*;



/**
 * Created by vladyslav on 10/8/17.
 */


public class Main {


    public enum Direction {

        NORTH, SOUTH, EAST, WEST;

        private Direction opposite;


           static {
               NORTH.opposite = SOUTH;
               SOUTH.opposite = NORTH;
               EAST.opposite = WEST;
               WEST.opposite = EAST;
           }




        public Direction getOppositeDirection() {
            return opposite;
        }


        public Direction getOpposite(){

               switch (this) {
                   case EAST : opposite = WEST;
                   case NORTH: opposite = SOUTH;
                   case SOUTH: opposite = NORTH;
                   case WEST: opposite = EAST;
               }
            return opposite;
        }

    }


    public static class TestJson {

        public static void  getData(String environment, String data) {


            String dataValue = null;


            JSONParser parser = new JSONParser();


            try {

                Object obj = parser.parse(new FileReader("/Users/vladyslav/IdeaProjects/cucumber/data"));

                JSONObject fileData = (JSONObject) obj;

               JSONObject environments = (JSONObject) fileData.get("environment");
               JSONObject specificEnvironment = (JSONObject) environments.get(environment);
               dataValue = (String)specificEnvironment.get(data);

            }
            catch (Exception e){
                e.printStackTrace();
            }
            System.out.println(dataValue);
        }




    }









}
